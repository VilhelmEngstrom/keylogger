# Keylogger

A simple keylogger for Linux. Reads whatever block device represents the keyboard (requires root) and sends the data via the POSIX socket API.

[![Build Status](https://gitlab.com/vilhelmengstrom/keylogger/badges/master/pipeline.svg)](https://gitlab.com/vilhelmengstrom/keylogger/commits/master)

### Disclaimer

This was written for educational purposes only. The keylogger is entirely harmless on a properly configured \*nix system unless logged in as root.
