#include "server.h"
#include "socket_utils.h"

#include <stdio.h>
#include <stdlib.h>

#include <arpa/inet.h>
#include <fcntl.h>
#include <netdb.h>
#include <netinet/in.h>
#include <signal.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <unistd.h>

#define LISTEN_BACKLOG 5

#ifndef __GNUC__
#error GNU C support is required
#endif

void clear_buffer(char* buffer, size_t size) {
    size_t i;
    for(i = 0; i < size; i++)
        buffer[i] = '\0';
}

int server_listen() {
    extern sig_atomic_t volatile keep_running;

    char buffer[SEND_BUF_SIZE];
    int client_sockfd;
    int server_sockfd __attribute__ ((__cleanup__(close_socket)));
    socklen_t client_len;
    ssize_t bytes_read;
    struct protoent* protoent;
    struct sockaddr_in client_addr, server_addr;

    protoent = getprotobyname(PROTOCOL);
    if(!protoent) {
        fprintf(stderr, "Failed to set protocol\n");
        return 1;
    }

    server_sockfd = socket(AF_INET, SOCK_STREAM, protoent->p_proto);

    if(server_sockfd == -1) {
        fprintf(stderr, "Failed to create socket\n");
        return 1;
    }

    server_addr.sin_family = AF_INET;
    server_addr.sin_addr.s_addr = htonl(INADDR_ANY);
    server_addr.sin_port = htons(PORT);

    if(bind(server_sockfd, (struct sockaddr*)&server_addr, sizeof(server_addr)) == -1) {
        fprintf(stderr, "Failed to bind socket\n");
        return 1;
    }

    if(listen(server_sockfd, LISTEN_BACKLOG) == -1) {
        fprintf(stderr, "Failed to listen on socket\n");
        return 1;
    }

    printf("Listening on port %d\n", PORT);

    client_len = sizeof(client_addr);
    while(keep_running) {
        client_sockfd = accept(server_sockfd, (struct sockaddr*)&client_addr, &client_len);

        do {
            bytes_read = read(client_sockfd, buffer, sizeof buffer);
            if(bytes_read > 0) {
                printf("%s\n", buffer);
            }
        } while(bytes_read > 0);

        close(client_sockfd);
        clear_buffer(buffer, sizeof buffer);
    }
    return 0;
}
