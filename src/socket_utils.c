#include "socket_utils.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <regex.h>
#include <unistd.h>

#define IP_ADDR_REGEX "^(([0-9]{1,3})\\.){3}[0-9]{1,3}$"

int is_valid_ip_addr(char const* ip) {
    regex_t regex;
    int reti, i, byte;

    reti = regcomp(&regex, IP_ADDR_REGEX, REG_EXTENDED);
    if(reti) {
        fprintf(stderr, "Failed to compile ip regex\n");
        return 0;
    }

    if(regexec(&regex, ip, 0, NULL, 0) != 0) {
        return 0;
    }

    for(i = 0; i < 4; i++) {
        byte = atoi(ip);

        if(byte < 0 || byte > 255) {
            return 0;
        }

        ip = strchr(ip, '.') + 1;
    }

    return 1;
}

void close_socket(int* fd) {
    close(*fd);
}
